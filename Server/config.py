import os
import copy
__model_name = [
'对话模型', '中文模型(通用)', '古代诗歌模型','散文现代诗歌模型','歌词模型'
]

__model_list = [
    "../GPT2/model/norm_model/chatmodel",
    "../GPT2/model/norm_model/chinesemodel",
    "../GPT2/model/norm_model/poertymodel",
    "../GPT2/model/norm_model/prosemodel",
    "../GPT2/model/norm_model/singermodel",
]

def flow_get_model_name_list():
    index = 0
    exits = []
    for path in copy.deepcopy(__model_list):
        if(os.path.exists(path)):
            exits.append(index)
        index+=1
    model_list = [__model_list[e] for e in exits ]
    model_name = [__model_name[e] for e in exits ]
    return model_name,model_list

def static_get_model_name_list():
    return __model_name,__model_list

